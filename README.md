# minecraftpi-gui
tkinter gui program to interact with minecraft pi edition

License GPL

Copyright 2016-2020  <Paul Sutton -zleap@disroot.org>

![screenshotdec2018](https://github.com/zleap/minecraftpi-gui/blob/master/minecraftpi-gui-dec2018.png)
This program is designed to integrate with minecraft pi

features :-
Post to chat- works
Get player position - works
goto specified position - in progress
flat world - runs a script to make the current world flat.  This can take a LONG TIME to complete.

Notes mcpi MUST be running an in an actual world for the program to connect and work

This version is hosted on the https://salsa.debian.org/ GitLab instance. 

<!--stackedit_data:
eyJoaXN0b3J5IjpbNzE2NzAwOTk1XX0=
-->


